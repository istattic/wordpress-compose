# Run WordPress from this Folder

Use this docker-compose file to run a simple wordress installation.  WordPress files are created or used from the `./wordpress` subfolder.

Provides access to wp-cli within the container for easy search-replace commands.  Builds off of the [tatemz/docker-wpcli](https://github.com/tatemz/docker-wpcli) image.

## Requirements

1. Install Docker and Docker Compose
2. Ensure your terminal has the docker-compose command available

Win PCs are supported using git-bash, though some docker commands require prefixing with winpty: `winpty docker ...`

## Usage

1. Copy `docker-compose.yaml` into a working folder, or clone this repo.
2. (Optionally) Copy the `env-example` file into `.env` to change config options
3. After staring the WordPress service for the first time, be sure to install
    > I.e., visit `http://localhost:8000` and finish the installer


#### Example Commands

Start the WordPress service.

>  `docker-compose up -d`

Stop the service, but keep database intact

> `docker-compose down`

Stop the service, and remove all database data

> `docker-compose down -v`

Get the name of the running container(s)

> `docker-compose ps`

#### Example WP-CLI Commands

_You can use the built-in WP-CLI to run commands within the container._

Get the name of the WordPress service container using `docker compose ps`, and pick the one with 'wordpress' in the name.  Use that container name for the rest of the commands in this list.

> `docker-compose ps`

Run a basic `wp-cli` against the WordPress installation.

> `docker exec -it container-name wp`

Search/Replace the URL for this WordPress installation

> `docker exec -it container-name wp search-replace http://old.address.com:8000 http://new.address.com:8001`

Export the database entirely to `./wordpress/wordpress.sql`

> `docker exec -it container-name wp db export`

## Options

Copy `env-example` to `.env` and change options as needed.


File: `.env`
```
PORT - specify localhost port to use
COMPOSE_PROJECT_NAME - Prefix container names with this instead of default
```

## Making an Alias for WP-CLI Commands

You can optionally create an alias in your terminal session to make these commands simpler:

> `alias wp="docker exec -it ` \``docker container ps -f name=wordpress --format '{{.Names}}'`\` `  wp"`

git-bash users on Win PC:


> `alias wp="winpty docker exec -it ` \``docker container ps -f name=wordpress --format '{{.Names}}'`\` `  wp"`

Note, this above command doesn't work well if you have more than one WordPress service running at the same time.  If you find this to be a big part of your day, you can try the slower docker-composer version with bash cutting, to get the proper name of the container.

> `docker-compose.exe ps | grep wordpress | cut -f 1 -d " "`



